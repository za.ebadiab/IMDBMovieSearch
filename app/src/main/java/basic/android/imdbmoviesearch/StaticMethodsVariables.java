package basic.android.imdbmoviesearch;

import android.content.Context;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

public class StaticMethodsVariables {


     public static void toast(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }


    public static void setImageWithGlide(Context mContext, String url, ImageView imgIcon) {
        Glide.with(mContext).load(url).into(imgIcon);
    }

}
