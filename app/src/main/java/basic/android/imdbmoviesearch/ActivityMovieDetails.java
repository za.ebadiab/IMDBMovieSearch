package basic.android.imdbmoviesearch;


import android.os.Bundle;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.orm.query.Condition;
import com.orm.query.Select;

import basic.android.imdbmoviesearch.customViews.MyTextView;
import basic.android.imdbmoviesearch.imdbModel.IMDBDetailModel;
import basic.android.imdbmoviesearch.model.MovieDetailModel;
import basic.android.imdbmoviesearch.model.MovieModel;
import cz.msebera.android.httpclient.Header;

import static basic.android.imdbmoviesearch.StaticMethodsVariables.setImageWithGlide;
import static basic.android.imdbmoviesearch.StaticMethodsVariables.toast;

public class ActivityMovieDetails extends BaseActivity {

    ImageView imgPoster;
    MyTextView txtTitle, txtType, txtYear, txtIMDBId, txtGenre, txtDirector, txtCountry, txtLanguage, txtPlot, txtActors, txtWebsite;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);

        bind();
        loadMovieDetail(getIntent().getStringExtra("IMDBID"));


    }


    public void loadMovieDetail(String IMDBID) {

        if (searchMovieDetailInDB(IMDBID)) {

            return;
        } else {
            searchMovieDetailInIMDB(IMDBID);
        }


    }

    public boolean searchMovieDetailInDB(String IMDBID) {

        if (dbCount() == 0) {
            return false;
        } else {

            MovieDetailModel movieDetailModel = Select.from(MovieDetailModel.class).where(Condition.prop("IMDB_Id").eq(IMDBID)).first();

            if (movieDetailModel != null) {
                setViewsByDBData(movieDetailModel);
                return true;
            } else {
                return false;
            }
        }
    }

    public void searchMovieDetailInIMDB(String IMDBID) {
        String movieDetail_searchByIMDBID__URL = "http://www.omdbapi.com/?i="
                + IMDBID
                + "&apikey=f6df2934";


        AsyncHttpClient client = new AsyncHttpClient();
        client.get(movieDetail_searchByIMDBID__URL, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                checkIMDBResponseForMovieDetailExists(responseString);

            }
       });

    }


    void checkIMDBResponseForMovieDetailExists(String responseString) {

        Gson gson = new Gson();
        IMDBDetailModel iMDBDetailModel = gson.fromJson(responseString, IMDBDetailModel.class);

        if (!iMDBDetailModel.getResponse().equals("True")) {

            toast(mContext, "Movie Detail not exists!");

        } else {
            saveMovieDetailOnDB(iMDBDetailModel);
        }
    }

    void saveMovieDetailOnDB(IMDBDetailModel iMDBDetailModel) {


        MovieDetailModel movieDetailModel = new MovieDetailModel();

        movieDetailModel.setIMDBId(iMDBDetailModel.getImdbID());
        movieDetailModel.setGenre(iMDBDetailModel.getGenre());
        movieDetailModel.setDirector(iMDBDetailModel.getDirector());
        movieDetailModel.setCountry(iMDBDetailModel.getCountry());
        movieDetailModel.setLanguage(iMDBDetailModel.getLanguage());
        movieDetailModel.setPlot(iMDBDetailModel.getPlot());
        movieDetailModel.setActors(iMDBDetailModel.getActors());
        movieDetailModel.setWebsite(iMDBDetailModel.getWebsite());

        MovieModel movieModel = Select.from(MovieModel.class).where(Condition.prop("imdb_ID").eq(movieDetailModel.getIMDBId())).first();
        movieDetailModel.setMovieModel(movieModel);

        movieDetailModel.save();

        setViewsByRespose(iMDBDetailModel);

    }


    public void setViewsByDBData(MovieDetailModel movieDetailModel) {


        setImageWithGlide(mContext, movieDetailModel.getMovieModel().getPoster(), imgPoster);
        txtTitle.setText(movieDetailModel.getMovieModel().getTitle());
        txtType.setText("Type: " + movieDetailModel.getMovieModel().getType());
        txtYear.setText("Year: " + movieDetailModel.getMovieModel().getYear());

        txtIMDBId.setText("IMDB: " + movieDetailModel.getIMDBId());
        txtGenre.setText("Genre: " + movieDetailModel.getGenre());
        txtDirector.setText("Director: " + movieDetailModel.getDirector());
        txtCountry.setText("Country: " + movieDetailModel.getCountry());
        txtLanguage.setText("Language: " + movieDetailModel.getLanguage());
        txtPlot.setText("Plot: " + movieDetailModel.getPlot());
        txtActors.setText("Actors: " + movieDetailModel.getActors());
        txtWebsite.setText("Website: " + movieDetailModel.getWebsite());

    }


    public void setViewsByRespose(IMDBDetailModel iMDBDetailModel) {

        setImageWithGlide(mContext, iMDBDetailModel.getPoster(), imgPoster);

        txtTitle.setText(iMDBDetailModel.getTitle());
        txtType.setText("Type: " + iMDBDetailModel.getType());
        txtYear.setText("Year: " + iMDBDetailModel.getYear());
        txtIMDBId.setText("IMDB: " + iMDBDetailModel.getImdbID());
        txtGenre.setText("Genre: " + iMDBDetailModel.getGenre());
        txtDirector.setText("Director: " + iMDBDetailModel.getDirector());
        txtCountry.setText("Country: " + iMDBDetailModel.getCountry());
        txtLanguage.setText("Language: " + iMDBDetailModel.getLanguage());
        txtPlot.setText("Plot: " + iMDBDetailModel.getPlot());
        txtActors.setText("Actors: " + iMDBDetailModel.getActors());
        txtWebsite.setText("Website: " + iMDBDetailModel.getWebsite());

    }

    public void bind() {
        imgPoster = findViewById(R.id.imgPoster);
        txtTitle = findViewById(R.id.txtTitle);
        txtType = findViewById(R.id.txtType);
        txtYear = findViewById(R.id.txtYear);
        txtIMDBId = findViewById(R.id.txtIMDBId);
        txtGenre = findViewById(R.id.txtGenre);
        txtDirector = findViewById(R.id.txtDirector);
        txtCountry = findViewById(R.id.txtCountry);
        txtLanguage = findViewById(R.id.txtLanguage);
        txtPlot = findViewById(R.id.txtPlot);
        txtActors = findViewById(R.id.txtActors);
        txtWebsite = findViewById(R.id.txtWebsite);
    }

    long dbCount() {
        return MovieDetailModel.count(MovieDetailModel.class, null, null);
    }
}
