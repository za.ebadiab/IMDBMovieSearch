package basic.android.imdbmoviesearch;


import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.List;

import basic.android.imdbmoviesearch.customViews.MyEditText;
import basic.android.imdbmoviesearch.imdbModel.IMDBModel;
import basic.android.imdbmoviesearch.imdbModel.Search;
import basic.android.imdbmoviesearch.model.MovieModel;
import cz.msebera.android.httpclient.Header;

import static basic.android.imdbmoviesearch.StaticMethodsVariables.toast;

public class MainActivity extends BaseActivity {

    MyEditText txtEnterMovieTitle;
    RecyclerView recyclerView;

    List<MovieModel> movieModelList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        bind();

    }


    void bind() {

        txtEnterMovieTitle = findViewById(R.id.txtEnterMovieTitle);
        recyclerView = findViewById(R.id.recyclerView);


        findViewById(R.id.btnSearch).setOnClickListener(v -> {

            if (!txtEnterMovieTitle.text().equals("")) {
                if (searchMovieInDB(txtEnterMovieTitle.text())) {
                    return;
                } else {
                    searchMovieListByTitle(txtEnterMovieTitle.text());
                }

            } else {
                toast(mContext, "Movie Title is Empty!");
            }


        });


    }


    void searchMovieListByTitle(String movieTitle) {

        String movieTitle_search__URL = "http://www.omdbapi.com/?s="
                + movieTitle
                + "&apikey=f6df2934";

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(movieTitle_search__URL, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {

                checkIMDBResponseForMovieExists(responseString);

            }


        });
    }

    void checkIMDBResponseForMovieExists(String responseString) {

        Gson gson = new Gson();
        IMDBModel iMDBModel = gson.fromJson(responseString, IMDBModel.class);

        if (!iMDBModel.getResponse().equals("True")) {
            toast(mContext, "Movie not exists!");

        } else {
            saveMovieListOnDB(iMDBModel.getSearch());

        }
        txtEnterMovieTitle.setText("");

    }


    void saveMovieListOnDB(List<Search> movieList) {

        movieModelList = new ArrayList<>();

        for (Search search : movieList) {

            MovieModel movieModel = new MovieModel();

            movieModel.setTitle(search.getTitle());
            movieModel.setImdbID(search.getImdbID());
            movieModel.setType(search.getType());
            movieModel.setYear(search.getYear());
            movieModel.setPoster(search.getPoster());
            movieModelList.add(movieModel);

            movieModel.save();
        }

        setAdapter();


    }

    public void setAdapter() {
        RecyclerMovieAdapter adapter = new RecyclerMovieAdapter(mContext, movieModelList);
        recyclerView.setAdapter(adapter);
        txtEnterMovieTitle.setText("");
    }

    public boolean searchMovieInDB(String movieTitle) {


        for (int i = 0; i < dbCount(); i++) {
            MovieModel movieModel = MovieModel.findById(MovieModel.class, i + 1);

        }


        String likeMovieTitle = "%" + movieTitle + "%";
        movieModelList = Select.from(MovieModel.class).where(Condition.prop("title").like(likeMovieTitle)).list();

        if (movieModelList.size() != 0) {
            setAdapter();
            return true;
        } else {

            return false;
        }

    }


    long dbCount() {
        return MovieModel.count(MovieModel.class, null, null);
    }

}
