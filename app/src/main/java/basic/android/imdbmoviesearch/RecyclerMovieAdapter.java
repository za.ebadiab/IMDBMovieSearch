package basic.android.imdbmoviesearch;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.List;

import basic.android.imdbmoviesearch.customViews.MyTextView;
import basic.android.imdbmoviesearch.model.MovieModel;

import static basic.android.imdbmoviesearch.StaticMethodsVariables.setImageWithGlide;

public class RecyclerMovieAdapter extends RecyclerView.Adapter<RecyclerMovieAdapter.Holder> {

    private Context mContext;

    private List<MovieModel> movieModelArrayList;

    RecyclerMovieAdapter(Context context, List<MovieModel> movieModelArrayList) {
        this.mContext = context;
        this.movieModelArrayList = movieModelArrayList;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.movie_recycler_item, parent, false);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        setImageWithGlide(mContext, movieModelArrayList.get(position).getPoster(), holder.imgPoster);

        holder.txtTitle.setText(movieModelArrayList.get(position).getTitle());
        holder.txtType.setText("Type: " + movieModelArrayList.get(position).getType());
        holder.txtYear.setText("Year: " + movieModelArrayList.get(position).getYear());
        holder.txtIMDBId.setText("IMDBID: " + movieModelArrayList.get(position).getImdbID());
    }

    @Override
    public int getItemCount() {
        return movieModelArrayList.size();
    }


    class Holder extends RecyclerView.ViewHolder {
        ImageView imgPoster;
        MyTextView txtTitle, txtType, txtYear, txtIMDBId;

        public Holder(View itemView) {
            super(itemView);

            imgPoster = itemView.findViewById(R.id.imgPoster);
            txtTitle = itemView.findViewById(R.id.txtTitle);
            txtType = itemView.findViewById(R.id.txtType);
            txtYear = itemView.findViewById(R.id.txtYear);
            txtIMDBId = itemView.findViewById(R.id.txtIMDBId);

            imgPoster.setOnClickListener(v -> {

                Intent intent = new Intent(mContext, ActivityMovieDetails.class);

                intent.putExtra("IMDBID", movieModelArrayList.get(getAdapterPosition()).getImdbID());
                mContext.startActivity(intent);
            });
        }


    }
}
