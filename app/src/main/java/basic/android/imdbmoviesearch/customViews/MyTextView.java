package basic.android.imdbmoviesearch.customViews;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

public class MyTextView extends AppCompatTextView {
    public MyTextView(Context context) {
        super(context);

        setFont(context);
    }

    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont(context);
    }


    void setFont(Context mContext) {
        Typeface timesNewRomanFont = Typeface.createFromAsset(mContext.getAssets(), "fonts/times.ttf");

        this.setTypeface(timesNewRomanFont);
    }

}
