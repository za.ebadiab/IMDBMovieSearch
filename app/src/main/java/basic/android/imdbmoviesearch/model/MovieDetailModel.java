package basic.android.imdbmoviesearch.model;

import com.orm.SugarRecord;

public class MovieDetailModel extends SugarRecord {


    private String IMDBId;
    private String genre;
    private String director;
    private String country;
    private String language;
    private String plot;
    private String actors;
    private String website;
    MovieModel movieModel;

    public MovieDetailModel() {
    }

    public MovieDetailModel(String IMDBId, String genre, String director, String country, String language, String plot, String actors, String website) {
        this.IMDBId = IMDBId;
        this.genre = genre;
        this.director = director;
        this.country = country;
        this.language = language;
        this.plot = plot;
        this.actors = actors;
        this.website = website;
    }


    public String getIMDBId() {
        return IMDBId;
    }

    public void setIMDBId(String IMDBId) {
        this.IMDBId = IMDBId;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public String getActors() {
        return actors;
    }

    public void setActors(String actors) {
        this.actors = actors;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public MovieModel getMovieModel() {
        return movieModel;
    }

    public void setMovieModel(MovieModel movieModel) {
        this.movieModel = movieModel;
    }
}
